lib.name = pdogg
class.sources = \
	oggamp~.c \
	oggcast~.c \
	oggread~.c \
	oggwrite~.c \
	$(empty)

lib.setup.sources = \
	pdogg.c \
	$(empty)

PKG_CONFIG ?= pkg-config
vorbiscflags = $(shell $(PKG_CONFIG) --cflags vorbis vorbisenc vorbisfile)
vorbislibs = $(shell $(PKG_CONFIG) --libs vorbis vorbisenc vorbisfile)

cflags = $(vorbiscflags)
ldlibs = $(vorbislibs)

datafiles = \
	$(wildcard *.pd) \
	$(wildcard *.txt) \
	$(wildcard *.md) \
	HISTORY \
	$(empty)

datadirs = \
	$(empty)

PDLIBBUILDER_DIR=/usr/share/pd-lib-builder
include $(PDLIBBUILDER_DIR)/Makefile.pdlibbuilder
