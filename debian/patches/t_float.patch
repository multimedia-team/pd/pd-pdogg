Description: Use Pd64-safe types
Author: IOhannes m zmölnig
Origin: Debian
Forwarded: no
Last-Update: 2024-06-17
---
This patch header follows DEP-3: http://dep.debian.net/deps/dep3/
Index: pd-pdogg-0.25.1/oggamp~.c
===================================================================
--- pd-pdogg-0.25.1.orig/oggamp~.c
+++ pd-pdogg-0.25.1/oggamp~.c
@@ -118,27 +118,27 @@ typedef struct _oggamp
 	t_clock  *x_clock;
 
     t_float *x_buf;    	    	    	    /* audio data buffer */
-    t_int x_bufsize;  	    	    	    /* buffer size in bytes */
-    t_int x_noutlets; 	    	    	    /* number of audio outlets */
+    int x_bufsize;  	    	    	    /* buffer size in bytes */
+    int x_noutlets; 	    	    	    /* number of audio outlets */
     t_sample **x_outvec;					/* audio vectors */
-    t_int x_vecsize;  	    	    	    /* vector size for transfers */
-    t_int x_state;    	    	    	    /* opened, running, or idle */
+    int x_vecsize;  	    	    	    /* vector size for transfers */
+    int x_state;    	    	    	    /* opened, running, or idle */
 
     	/* parameters to communicate with subthread */
-    t_int x_requestcode;	   /* pending request from parent to I/O thread */
-    t_int x_connecterror;	   /* slot for "errno" return */
-    t_int x_streamchannels;	   /* number of channels in Ogg Vorbis bitstream */
-	t_int x_streamrate;        /* sample rate of stream */
+    int x_requestcode;	   /* pending request from parent to I/O thread */
+    int x_connecterror;	   /* slot for "errno" return */
+    int x_streamchannels;	   /* number of channels in Ogg Vorbis bitstream */
+	int x_streamrate;        /* sample rate of stream */
 
 		/* buffer stuff */
-    t_int x_fifosize; 	       /* buffer size appropriately rounded down */	    
-    t_int x_fifohead; 	       /* index of next byte to get from file */
-    t_int x_fifotail; 	       /* index of next byte the ugen will read */
-	t_int x_fifobytes;         /* number of bytes available in buffer */
-    t_int x_eof;   	           /* true if ogg stream has ended */
-    t_int x_sigcountdown;      /* counter for signalling child for more data */
-    t_int x_sigperiod;	       /* number of ticks per signal */
-	t_int x_siginterval;       /* number of times per buffer (depends on data rate) */
+    int x_fifosize; 	       /* buffer size appropriately rounded down */	    
+    int x_fifohead; 	       /* index of next byte to get from file */
+    int x_fifotail; 	       /* index of next byte the ugen will read */
+	int x_fifobytes;         /* number of bytes available in buffer */
+    int x_eof;   	           /* true if ogg stream has ended */
+    int x_sigcountdown;      /* counter for signalling child for more data */
+    int x_sigperiod;	       /* number of ticks per signal */
+	int x_siginterval;       /* number of times per buffer (depends on data rate) */
 
 		/* ogg/vorbis related stuff */
 	ogg_stream_state x_os;     /* take physical pages, weld into a logical stream of packets */
@@ -149,27 +149,27 @@ typedef struct _oggamp
 	vorbis_comment   x_vc;     /* struct that stores all the user comments */
 	vorbis_dsp_state x_vd;     /* central working state for the packet->PCM decoder */
 	vorbis_block     x_vb;     /* local working space for packet->PCM decode */
-	t_int            x_eos;    /* end of stream */
+	int            x_eos;    /* end of stream */
     char            *x_buffer; /* buffer used to pass on to ogg/vorbis */
 
-    t_int     x_vorbis;        /* info about encoder status */
-	t_int     x_sync;          /* indicates whether the decoder has been synced */
+    int     x_vorbis;        /* info about encoder status */
+	int     x_sync;          /* indicates whether the decoder has been synced */
 	t_float   x_pages;         /* number of pages that have been output to server */
     t_outlet *x_outpages;      /* output to send them to */
 
 
-	t_int    x_connectstate;   /* indicates the state of socket connection */
-    t_int    x_fd;             /* the socket number */
-    t_int    x_graphic;        /* indicates if we show a graphic bar */ 
+	int    x_connectstate;   /* indicates the state of socket connection */
+    int    x_fd;             /* the socket number */
+    int    x_graphic;        /* indicates if we show a graphic bar */ 
 	t_float  x_resample;       /* indicates if we need to resample signal (1 = no resampling) */
-	t_int    x_recover;        /* indicate how to behave on buffer underruns */
-	t_int    x_disconnect;     /* indicates that user want's to disconnect */
-    t_int    x_samplerate;     /* Pd's sample rate */
+	int    x_recover;        /* indicate how to behave on buffer underruns */
+	int    x_disconnect;     /* indicates that user want's to disconnect */
+    int    x_samplerate;     /* Pd's sample rate */
 
 		/* server stuff */
 	const char    *x_hostname;       /* name or IP of host to connect to */
 	const char    *x_mountpoint;     /* mountpoint of ogg-bitstream */
-	t_int    x_port;           /* port number on which the connection is made */
+	int    x_port;           /* port number on which the connection is made */
 
 		/* tread stuff */
     pthread_mutex_t   x_mutex;
@@ -179,12 +179,12 @@ typedef struct _oggamp
 } t_oggamp;
 
 	/* check if we can read from socket */
-static int oggamp_check_for_data(t_int sock)
+static int oggamp_check_for_data(int sock)
 {
 
 	fd_set set;
 	struct timeval tv;
-	t_int ret;
+	int ret;
 
 	tv.tv_sec = 0;
 	tv.tv_usec = 20000;
@@ -373,7 +373,7 @@ static void oggamp_vorbis_deinit(t_oggam
 }
 
 	/* decode ogg/vorbis and receive new data */
-static int oggamp_decode_input(t_oggamp *x, float *buf, int fifohead, int fifosize, int fd)
+static int oggamp_decode_input(t_oggamp *x, t_float *buf, int fifohead, int fifosize, int fd)
 {
 	int i, result;
 
@@ -488,7 +488,7 @@ static int oggamp_decode_input(t_oggamp
 }
   
     /* connect to shoutcast server */
-static int oggamp_child_connect(t_oggamp*x, const char *hostname, const char *mountpoint, t_int portno)
+static int oggamp_child_connect(t_oggamp*x, const char *hostname, const char *mountpoint, int portno)
 {
     struct          sockaddr_in server;
     struct          hostent *hp;
@@ -499,11 +499,11 @@ static int oggamp_child_connect(t_oggamp
 	char            *url;               /* used for relocation */
     fd_set          fdset;
     struct timeval  tv;
-    t_int           sockfd;                         /* socket to server */
-    t_int           relocate, numrelocs = 0;
-    t_int           i, ret, rest, nanswers=0;
+    int           sockfd;                         /* socket to server */
+    int           relocate, numrelocs = 0;
+    int           i, ret, rest, nanswers=0;
     char            *cpoint = NULL;
-	t_int           eof = 0;
+	int           eof = 0;
 
     sockfd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
     if (sockfd < 0)
@@ -672,7 +672,7 @@ static void oggamp_child_delgraphics(t_o
     }
 }
 
-static void oggamp_child_disconnect(t_int fd)
+static void oggamp_child_disconnect(int fd)
 {
     sys_closesocket(fd);
 	post("oggamp~: connection closed");
@@ -728,7 +728,7 @@ static void *oggamp_child_main(void *zz)
     {
     	int fd, fifohead;
 		char *buffer;	/* Ogg Vorbis data */
-		float *buf;		/* encoded PCM floats */
+		t_float *buf;		/* encoded PCM floats */
 		pute("0\n");
 		if (x->x_requestcode == REQUEST_NOTHING)
 		{
@@ -747,7 +747,7 @@ static void *oggamp_child_main(void *zz)
 			relinquish the mutex while we're in oggcast_child_connect(). */
 			const char *hostname = x->x_hostname;
 			const char *mountpoint = x->x_mountpoint;
-			t_int portno = x->x_port;
+			int portno = x->x_port;
 			x->x_disconnect = 0;
 	    		/* alter the request code so that an ensuing "open" will get
 			noticed. */
@@ -1005,7 +1005,7 @@ static void *oggamp_new(t_floatarg fdogr
 {
     t_oggamp *x;
     int nchannels = fnchannels, bufsize = fbufsize * 1024, i;
-    float *buf;
+    t_float *buf;
     
     if (nchannels < 1)
     	nchannels = 2;		/* two channels as default */
@@ -1067,13 +1067,13 @@ static void *oggamp_new(t_floatarg fdogr
 static t_int *oggamp_perform(t_int *w)
 {
     t_oggamp *x = (t_oggamp *)(w[1]);
-    t_int vecsize = x->x_vecsize, noutlets = x->x_noutlets, i, j, r;
+    int vecsize = x->x_vecsize, noutlets = x->x_noutlets, i, j, r;
     t_float *fp;
 	t_float *buffer = x->x_buf;
 
     if (x->x_state == STATE_STREAM)
     {
-    	t_int wantbytes, getbytes, havebytes, nchannels, streamchannels = x->x_streamchannels;
+    	int wantbytes, getbytes, havebytes, nchannels, streamchannels = x->x_streamchannels;
 
 		pthread_mutex_lock(&x->x_mutex);
 
@@ -1219,8 +1219,8 @@ static void oggamp_connect_url(t_oggamp
 	const char *portptr;
 	char *p0, *p_;
 	const char *defaultportstr = "8000";
-	t_int stringlength;
-	t_int portno;
+	int stringlength;
+	int portno;
 
 		/* strip http:// or ftp:// */
 	p = url->s_name;
Index: pd-pdogg-0.25.1/oggcast~.c
===================================================================
--- pd-pdogg-0.25.1.orig/oggcast~.c
+++ pd-pdogg-0.25.1/oggcast~.c
@@ -114,30 +114,30 @@ static t_class *oggcast_class;
 typedef struct _oggcast
 {
     t_object x_obj;
-    t_float *x_f;
+    t_float x_f;
     t_clock *x_clock_connect;
     t_clock *x_clock_pages;
     t_outlet *x_connection;    /* outlet for connection state */
     t_outlet *x_outpages;	   /* outlet for no. of ogg pages */
 
     t_float *x_buf;    	    	    	    /* audio data buffer */
-    t_int x_bufsize;  	    	    	    /* buffer size in bytes */
-    t_int x_ninlets; 	    	    	    /* number of audio outlets */
+    int x_bufsize;  	    	    	    /* buffer size in bytes */
+    int x_ninlets; 	    	    	    /* number of audio outlets */
     t_sample **x_outvec;	/* audio vectors */
-    t_int x_vecsize;  	    	    	    /* vector size for transfers */
-    t_int x_state;    	    	    	    /* opened, running, or idle */
+    int x_vecsize;  	    	    	    /* vector size for transfers */
+    int x_state;    	    	    	    /* opened, running, or idle */
 
     	/* parameters to communicate with subthread */
-    t_int x_requestcode;	   /* pending request from parent to I/O thread */
-    t_int x_connecterror;	   /* slot for "errno" return */
+    int x_requestcode;	   /* pending request from parent to I/O thread */
+    int x_connecterror;	   /* slot for "errno" return */
 
 		/* buffer stuff */
-    t_int x_fifosize; 	       /* buffer size appropriately rounded down */	    
-    t_int x_fifohead; 	       /* index of next byte to get from file */
-    t_int x_fifotail; 	       /* index of next byte the ugen will read */
-    t_int x_sigcountdown;      /* counter for signalling child for more data */
-    t_int x_sigperiod;	       /* number of ticks per signal */
-	t_int x_siginterval;       /* number of times per buffer (depends on data rate) */
+    int x_fifosize; 	       /* buffer size appropriately rounded down */	    
+    int x_fifohead; 	       /* index of next byte to get from file */
+    int x_fifotail; 	       /* index of next byte the ugen will read */
+    int x_sigcountdown;      /* counter for signalling child for more data */
+    int x_sigperiod;	       /* number of ticks per signal */
+	int x_siginterval;       /* number of times per buffer (depends on data rate) */
 
 		/* ogg/vorbis related stuff */
 	ogg_stream_state x_os;    /* take physical pages, weld into a logical stream of packets */
@@ -148,23 +148,23 @@ typedef struct _oggcast
 	vorbis_dsp_state x_vd;    /* central working state for the packet->PCM decoder */
 	vorbis_block     x_vb;    /* local working space for packet->PCM decode */
 
-	t_int            x_eos;   /* end of stream */
+	int            x_eos;   /* end of stream */
 	t_float   x_pages;        /* number of pages that have been output to server */
 	t_float   x_lastpages;
 
         /* ringbuffer stuff */
     t_float *x_buffer;        /* data to be buffered (ringbuffer)*/
-    t_int    x_bytesbuffered; /* number of unprocessed bytes in buffer */
+    int    x_bytesbuffered; /* number of unprocessed bytes in buffer */
 
         /* ogg/vorbis format stuff */
-    t_int    x_samplerate;    /* samplerate of stream (default = getsr() ) */
-	t_int    x_skip;          /* samples from input to skip (for resampling) */
+    int    x_samplerate;    /* samplerate of stream (default = getsr() ) */
+	int    x_skip;          /* samples from input to skip (for resampling) */
 	t_float  x_quality;       /* desired quality level from 0.0 to 1.0 (lo to hi) */
-    t_int    x_br_max;        /* max. bitrate of ogg/vorbis stream */
-    t_int    x_br_nom;        /* nom. bitrate of ogg/vorbis stream */
-    t_int    x_br_min;        /* min. bitrate of ogg/vorbis stream */
-    t_int    x_channels;      /* number of channels (1 or 2) */
-	t_int    x_vbr;
+    int    x_br_max;        /* max. bitrate of ogg/vorbis stream */
+    int    x_br_nom;        /* nom. bitrate of ogg/vorbis stream */
+    int    x_br_min;        /* min. bitrate of ogg/vorbis stream */
+    int    x_channels;      /* number of channels (1 or 2) */
+	int    x_vbr;
 
         /* IceCast server stuff */
     const char*    x_passwd;        /* password for server */
@@ -181,17 +181,17 @@ typedef struct _oggcast
 	const char*    x_hostname;      /* name or IP of host to connect to */
 	const char*    x_mountpoint;    /* mountpoint for IceCast server */
 	t_float  x_port;          /* port number on which the connection is made */
-    t_int    x_bcpublic;      /* do(n't) publish broadcast on www.oggcast.com */
-	t_int    x_servertype;    /* type of server: 0 = JRoar or old Icecast2; 1 = new Icecast2 */
+    int    x_bcpublic;      /* do(n't) publish broadcast on www.oggcast.com */
+	int    x_servertype;    /* type of server: 0 = JRoar or old Icecast2; 1 = new Icecast2 */
 
 	
 	
 
-	t_int    x_connectstate;   /* indicates to state of socket connection */
-	t_int    x_outvalue;       /* value that has last been output via outlet */
-    t_int    x_fd;             /* the socket number */
+	int    x_connectstate;   /* indicates to state of socket connection */
+	int    x_outvalue;       /* value that has last been output via outlet */
+    int    x_fd;             /* the socket number */
 	t_resample    x_resample;  /* resampling unit */
-	t_int    x_recover;        /* indicate how to behave on buffer underruns */
+	int    x_recover;        /* indicate how to behave on buffer underruns */
 
 		/* thread stuff */
     pthread_mutex_t   x_mutex;
@@ -244,7 +244,7 @@ char *oggcast_util_base64_encode(char *d
 }
 
 	/* check server for writeability */
-static int oggcast_checkserver(t_int sock)
+static int oggcast_checkserver(int sock)
 {
     fd_set          fdset;
     struct timeval  ztout;
@@ -273,7 +273,7 @@ static int oggcast_checkserver(t_int soc
 }
 
     /* stream ogg/vorbis to IceCast2 server */
-static int oggcast_stream(t_oggcast *x, t_int fd)
+static int oggcast_stream(t_oggcast *x, int fd)
 {
     int err = -1;            /* error return code */
 	int pages = 0;
@@ -445,7 +445,7 @@ static void oggcast_vorbis_deinit(t_oggc
 }
 
 	/* encode ogg/vorbis and stream new data */
-static int oggcast_encode(t_oggcast *x, float *buf, int channels, int fifosize, int fd)
+static int oggcast_encode(t_oggcast *x, t_float *buf, int channels, int fifosize, int fd)
 {
     unsigned short i, ch;
     int err = 0;
@@ -459,7 +459,7 @@ static int oggcast_encode(t_oggcast *x,
 	{
 		for(ch = 0; ch < channels; ch++)
 		{
-			inbuffer[ch][n] = *buf++;
+			inbuffer[ch][n] = (float)*buf++;
 		}
 	}
 		/* tell the library how much we actually submitted */
@@ -490,9 +490,9 @@ static int oggcast_encode(t_oggcast *x,
 }
   
     /* connect to icecast2 server */
-static int oggcast_child_connect(t_oggcast*x, const char *hostname, const char *mountpoint, t_int portno, 
+static int oggcast_child_connect(t_oggcast*x, const char *hostname, const char *mountpoint, int portno, 
 								 const char *passwd, char *bcname, char *bcurl,
-								 char *bcgenre, t_int bcpublic, t_int br_nom, t_int servertype)
+								 char *bcgenre, int bcpublic, int br_nom, int servertype)
 {
     struct          sockaddr_in server;
     struct          hostent *hp;
@@ -503,8 +503,8 @@ static int oggcast_child_connect(t_oggca
     unsigned int    len;
     fd_set          fdset;
     struct timeval  tv;
-    t_int           sockfd;                         /* our internal handle for the socket */
-    t_int           ret;
+    int           sockfd;                         /* our internal handle for the socket */
+    int           ret;
 
     sockfd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
     if (sockfd < 0)
@@ -697,7 +697,7 @@ static int oggcast_child_connect(t_oggca
 
 
 
-static void oggcast_child_disconnect(t_int fd)
+static void oggcast_child_disconnect(int fd)
 {
     sys_closesocket(fd);
 	post("oggcast~: connection closed");
@@ -771,14 +771,14 @@ static void *oggcast_child_main(void *zz
 			relinquish the mutex while we're connecting to server. */
 			const char *hostname = x->x_hostname;
 			const char *mountpoint = x->x_mountpoint;
-			t_int portno = x->x_port;
+			int portno = x->x_port;
 			const char *passwd = x->x_passwd;
 			char *bcname = x->x_bcname;
 			char *bcgenre = x->x_bcgenre;
 			char *bcurl = x->x_bcurl;
-			t_int bcpublic = x->x_bcpublic;
-			t_int br_nom = x->x_br_nom;
-			t_int servertype = x->x_servertype;
+			int bcpublic = x->x_bcpublic;
+			int br_nom = x->x_br_nom;
+			int servertype = x->x_servertype;
 	    		/* alter the request code so that an ensuing "open" will get
 			noticed. */
     			pute("4\n");
@@ -854,7 +854,7 @@ static void *oggcast_child_main(void *zz
 			while (x->x_requestcode == REQUEST_BUSY)
 			{
 	    		int fifosize = x->x_fifosize, fifotail, channels;
-				float *buf = x->x_buf;
+				t_float *buf = x->x_buf;
     	    	pute("77\n");
 
 				/* if the head is < the tail, we can immediately write
@@ -999,7 +999,7 @@ static void *oggcast_new(t_floatarg fnch
 {
     t_oggcast *x;
     int nchannels = fnchannels, bufsize = fbufsize * 1024, i;
-    float *buf;
+    t_float *buf;
     
     if (nchannels < 1)
     	nchannels = 2;		/* two channels as default */
@@ -1083,7 +1083,7 @@ static t_int *oggcast_perform(t_int *w)
 {
     t_oggcast *x = (t_oggcast *)(w[1]);
     int vecsize = x->x_vecsize, ninlets = x->x_ninlets, channels = x->x_channels, i, j, skip = x->x_skip;
-	float *sp = x->x_buf;
+	t_float *sp = x->x_buf;
 	pthread_mutex_lock(&x->x_mutex);
     if (x->x_state != STATE_IDLE)
     {
@@ -1246,7 +1246,7 @@ static void oggcast_password(t_oggcast *
     pthread_mutex_unlock(&x->x_mutex);
 }
     /* set comment fields for header (reads in just anything) */
-static void oggcast_comment(t_oggcast *x, t_symbol *s, t_int argc, t_atom* argv)
+static void oggcast_comment(t_oggcast *x, t_symbol *s, int argc, t_atom* argv)
 {
 	t_binbuf *b = binbuf_new();
 	char* comment;
@@ -1326,9 +1326,9 @@ static void oggcast_vbr(t_oggcast *x, t_
 {
     pthread_mutex_lock(&x->x_mutex);
     x->x_vbr = 1;
-	x->x_samplerate = (t_int)fsr;
+	x->x_samplerate = (int)fsr;
 	x->x_quality = fquality;
-	x->x_channels = (t_int)fchannels;
+	x->x_channels = (int)fchannels;
 	post("oggcast~: %d channels @ %d Hz, quality %.2f", x->x_channels, x->x_samplerate, x->x_quality);
 	if(x->x_state == STATE_STREAM)
 	{
@@ -1345,11 +1345,11 @@ static void oggcast_vorbis(t_oggcast *x,
 {
     pthread_mutex_lock(&x->x_mutex);
     x->x_vbr = 0;
-	x->x_samplerate = (t_int)fsr;
-	x->x_channels = (t_int)fchannels;
-	x->x_br_max = (t_int)fmax;
-	x->x_br_nom = (t_int)fnom;
-	x->x_br_min = (t_int)fmin;
+	x->x_samplerate = (int)fsr;
+	x->x_channels = (int)fchannels;
+	x->x_br_max = (int)fmax;
+	x->x_br_nom = (int)fnom;
+	x->x_br_min = (int)fmin;
 	post("oggcast~: %d channels @ %d Hz, bitrates: max. %d / nom. %d / min. %d", 
 		  x->x_channels, x->x_samplerate, x->x_br_max, x->x_br_nom, x->x_br_min);
 	if(x->x_state == STATE_STREAM)
Index: pd-pdogg-0.25.1/oggread~.c
===================================================================
--- pd-pdogg-0.25.1.orig/oggread~.c
+++ pd-pdogg-0.25.1/oggread~.c
@@ -92,7 +92,7 @@ typedef struct _oggread
     t_int    x_blocksize;     /* size of a dsp block */    
 	t_int    x_decoded;       /* number of samples we got from decoder on last call */
 
-    t_float *x_outbuffer;     /* buffer to store audio decoded data */
+    t_sample *x_outbuffer;     /* buffer to store audio decoded data */
     t_int    x_outwriteposition;
     t_int    x_outreadposition;
     t_int    x_outunread;
@@ -164,8 +164,8 @@ static int oggread_decode_input(t_oggrea
 static t_int *oggread_perform(t_int *w)
 {
 	t_oggread *x = (t_oggread*) (w[1]);
-	t_float *out1 = (t_float *)(w[2]);
-	t_float *out2 = (t_float *)(w[3]);
+	t_sample *out1 = (t_sample *)(w[2]);
+	t_sample *out2 = (t_sample *)(w[3]);
 	int n = (int)(w[4]);
 	int ret;
 	int i = 0;
@@ -358,7 +358,7 @@ static void oggread_free(t_oggread *x)
 		ov_clear(&x->x_ov);
         x->x_fd = -1;
     }
-    freebytes(x->x_outbuffer, OUTPUT_BUFFER_SIZE*sizeof(t_float));
+    freebytes(x->x_outbuffer, OUTPUT_BUFFER_SIZE*sizeof(t_sample));
 	clock_free(x->x_clock);
 }
 
@@ -380,7 +380,7 @@ static void *oggread_new(t_floatarg fdog
     x->x_samplerate = sys_getsr();
 
     x->x_outbuffersize = OUTPUT_BUFFER_SIZE; 
-    x->x_outbuffer = (t_float*) getbytes(OUTPUT_BUFFER_SIZE*sizeof(t_float));
+    x->x_outbuffer = (t_sample*) getbytes(OUTPUT_BUFFER_SIZE*sizeof(t_sample));
 
     if(!x->x_outbuffer)
     {
